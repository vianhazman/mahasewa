from django.contrib import admin
from .models import *
from django.utils.translation import ugettext_lazy
# Register your models here.
admin.site.register(Contact)
admin.site.register(Testimonial)

admin.site.site_header = 'Mahasewa Admin'

class ImageInline_Room(admin.StackedInline):
    model = RoomImage


class FacilitesInline_Room(admin.StackedInline):
    model = RoomFacility

class FacilitiesInline_Apt(admin.StackedInline):
    model = AptFacility

@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ("no","apartment","tower","floor",)
    search_fields = ("no","apartment","tower","floor",)
    model = RoomImage
    inlines = [
        FacilitesInline_Room,
        ImageInline_Room,
    ]
RoomAdmin.DescTextbox = "Description"

@admin.register(Apartment)
class ApartmentAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)
    inlines = [
    FacilitiesInline_Apt,
    ]
