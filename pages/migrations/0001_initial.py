# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-07-17 10:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Apartment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(max_length=20)),
                ('Address', models.TextField()),
                ('descTextbox', models.TextField()),
                ('facilityTextbox', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(max_length=20)),
                ('PhoneNum', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('No', models.CharField(max_length=20)),
                ('Tower', models.CharField(max_length=1)),
                ('Floor', models.CharField(max_length=20)),
                ('descTextbox', models.TextField()),
                ('facilityTextbox', models.TextField()),
                ('pricePlaceholder', models.CharField(max_length=20)),
                ('Apartment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pages.Apartment')),
            ],
        ),
        migrations.CreateModel(
            name='Testimonials',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Name', models.CharField(max_length=20)),
                ('Role', models.CharField(max_length=20)),
                ('textBox', models.TextField()),
                ('image', models.ImageField(upload_to='static/images/testimonial')),
            ],
        ),
    ]
