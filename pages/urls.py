from django.conf.urls import url
from .views import *
from django.conf import settings
from django.conf.urls.static import static
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/', about, name='about'),
    url(r'^katalog/', katalog, name='katalog'),
    url(r'^apartment/(?P<aptId>[0-9]+)',apartment, name='apartment'),
    url(r'^room/(?P<roomId>[0-9]+)',room, name='room')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
