from django.db import models

# Create your models here.
class Apartment(models.Model):
	name = models.CharField(max_length=20, blank=False)
	address = models.TextField(blank=False)
	descTextbox = models.TextField(blank=False,verbose_name='Description')
	image = models.ImageField(upload_to = "static/images/apartment", default=" " )
	location = models.CharField(max_length=50, blank=False, default="not set")
	def __str__(self):
		return self.name

class Room(models.Model):
	no = models.CharField(max_length=20, blank=False)
	apartment = models.ForeignKey(Apartment, on_delete=models.CASCADE)
	tower     = models.CharField(max_length=1)
	floor = models.CharField(max_length=20, blank=False)
	descTextbox = models.TextField(blank=False,verbose_name='Description')
	pricePlaceholder = models.CharField(max_length=20, blank=False,verbose_name='Price')

	def __str__(self):
		return self.no

class RoomImage(models.Model):
	room = models.ForeignKey(Room, related_name='images', on_delete=models.CASCADE)
	image = models.ImageField(upload_to = "static/images/room" )

class AptFacility(models.Model):
	apartment = models.ForeignKey(Apartment, related_name='facility', on_delete=models.CASCADE)
	facilityTextbox = models.CharField(max_length=20, blank=False,verbose_name='Facility')
	image = models.ImageField(upload_to = "static/images/facilityLogo", default= "null")

class RoomFacility(models.Model):
	apartment = models.ForeignKey(Room, related_name='facility', on_delete=models.CASCADE)
	facilityTextbox = models.CharField(max_length=20, blank=False,verbose_name='Facility')
	image = models.ImageField(upload_to = "static/images/facilityLogo", default= "null" )
class Contact(models.Model):
	Name = models.CharField(max_length=20, blank=False)
	phoneNum = models.IntegerField()

class Testimonial(models.Model):
	Name = models.CharField(max_length=20, blank=False)
	role = models.CharField(max_length=20, blank=False)
	textBox = models.TextField(blank=False, verbose_name='Description')
	image = models.ImageField(upload_to = "static/images/testimonial" )

	def __str__(self):
		return self.Name
